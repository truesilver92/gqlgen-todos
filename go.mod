module gitlab.com/truesilver92/gqlgen-todos

go 1.13

require (
	github.com/99designs/gqlgen v0.9.3
	github.com/mdempsky/gocode v0.0.0-20190203001940-7fb65232883f // indirect
	github.com/vektah/gqlparser v1.1.2
	golang.org/x/tools v0.0.0-20190906203814-12febf440ab1 // indirect
)
